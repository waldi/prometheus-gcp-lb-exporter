// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
)

type sink interface {
	Message(context.Context, *LogMsg)
}
