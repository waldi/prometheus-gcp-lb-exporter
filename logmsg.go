// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"net/url"
	"time"
)

type LogMsgHttpRequest struct {
	Protocol string `json:"protocol"`
	RemoteIp string `json:"remoteIp"`
	RequestMethod string `json:"requestMethod"`
	RequestSize string `json:"requestSize"`
	RequestUrl string `json:"requestUrl"`
	ResponseSize string `json:"responseSize"`
	Status int `json:"status"`
	UserAgent string `json:"userAgent"`
	CacheHit bool `json:"cacheHit"`
}

type LogMsgJsonPayload struct {
	StatusDetails string `json:"statusDetails"`
}

type LogMsgResource struct {
	Labels map[string]string `json:"labels"`
	Type string `json:"type"`
}

type LogMsgInfo struct {
	Cache string
	Url *url.URL
	Urlid string
}

type LogMsg struct {
	HttpRequest LogMsgHttpRequest `json:"httpRequest"`
	JsonPayload LogMsgJsonPayload `json:"jsonPayload"`
	Resource LogMsgResource `json:"resource"`
	Severity string `json:"severity"`
	Timestamp time.Time `json:"timestamp"`
	Info LogMsgInfo `json:"-"`
}
