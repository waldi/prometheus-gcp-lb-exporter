// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"log"
	"syscall"

	"github.com/spf13/cobra"
)

var (
	cloudProject string
	cloudSubscription string
	cloudTopic string
	logFile string
	logStdout bool
	listen string
	mapUrlid mapUrl = make(mapUrl)
)

func run(cmd *cobra.Command, args []string) {
	ctx := context.Background()

	var sinks []sink
	sinks = append(sinks, newSinkPrometheus(listen, syscall.SIGUSR1))
	if logFile != "" {
		sink, err := newSinkLogFile(logFile, syscall.SIGUSR1)
		if err != nil {
			log.Fatal("Failed to open log file: ", err)
		}
		sinks = append(sinks, sink)
	}
	if logStdout {
		sinks = append(sinks, newSinkLog())
	}

	err := fetchLog(cloudProject, cloudTopic, cloudSubscription, ctx, sinks...)
	log.Fatal(err)
}

var cmd = &cobra.Command{
	Use: "prometheus-gcp-lb-exporter",
	Run: run,
}

func init() {
	cmd.Flags().StringVar(&cloudProject, "cloud-project", "", "Google Cloud project")
	cmd.Flags().StringVar(&cloudSubscription, "cloud-subscription", "", "Google Cloud Pub/Sub subscription")
	cmd.Flags().StringVar(&cloudTopic, "cloud-topic", "", "Google Cloud Pub/Sub topic to create subscription on")
	cmd.Flags().StringVar(&listen, "listen", "127.0.0.1:9121", "Address to listen on")
	cmd.Flags().StringVar(&logFile, "log-file", "", "Write a web server log to file (use SIGUSR1 to re-open)")
	cmd.Flags().BoolVar(&logStdout, "log-stdout", false, "Write a web server log to stdout")
	cmd.Flags().Var(mapUrlid, "map-urlid", "Map URL pattern to prometheus label \"urlid\"")
}

func main() {
	cmd.Execute()
}
