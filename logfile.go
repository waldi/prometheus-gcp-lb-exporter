// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"log"
	"os"
	"os/signal"
	"sync"
)

type Logfile struct {
	file *os.File
	mutex sync.Mutex
	path string
	notify chan os.Signal
}

func NewLogfile(path string, sig os.Signal) (*Logfile, error) {
	file := &Logfile{
		mutex: sync.Mutex{},
		path: path,
		notify: make(chan os.Signal, 1),
	}

	if err := file.reopen(); err != nil {
		return nil, err
	}

	go func() {
		signal.Notify(file.notify, sig)

		for _ = range file.notify {
			if err := file.reopen(); err != nil {
				log.Fatal("Failed to re-open log file: ", err)
			} else {
				log.Print("Log file re-opened: ", path)
			}
		}
	}()

	return file, nil
}

func (self *Logfile) reopen() (err error) {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	self.file.Close()
	self.file, err = os.OpenFile(self.path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	return
}

func (self *Logfile) Write(b []byte) (int, error) {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	return self.file.Write(b)
}

func (self *Logfile) Close() error {
	self.mutex.Lock()
	defer self.mutex.Unlock()
	signal.Stop(self.notify)
	close(self.notify)
	return self.file.Close()
}
