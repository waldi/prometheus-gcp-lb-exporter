// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"encoding/json"
	"log"
	"net/url"
	"time"

	"cloud.google.com/go/pubsub"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func fetchLog(project, topic, subscription string, ctx context.Context, sinks ...sink) (err error) {
	client, err := pubsub.NewClient(ctx, project)
	if err != nil {
		return
	}

	top := client.Topic(topic)

	sub, err := client.CreateSubscription(ctx, subscription, pubsub.SubscriptionConfig{
		Topic:       top,
		AckDeadline: 10 * time.Second,
	})
	if err != nil {
		s, _ := status.FromError(err)
		if s.Code() != codes.AlreadyExists {
			log.Fatal(err)
		}
	}

	for true {
		err = sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
			msg.Ack()

			var msgdata LogMsg
			err := json.Unmarshal(msg.Data, &msgdata)

			url, err := url.Parse(msgdata.HttpRequest.RequestUrl)
			msgdata.Info.Url = url
			msgdata.Info.Urlid = mapUrlid.match(url)

			cache := "unknown"
			switch msgdata.JsonPayload.StatusDetails {
			case "failed_to_connect_to_backend":
				cache = "error"
			default:
				switch msgdata.HttpRequest.CacheHit {
				case true:
					cache = "hit"
				default:
					cache = "miss"
				}
			}
			msgdata.Info.Cache = cache

			if err != nil {
				log.Printf("Got error: %s", err)
			}

			for _, s := range sinks {
				s.Message(ctx, &msgdata)
			}
		})

		s, _ := status.FromError(err)
		switch s.Code() {
		case codes.Canceled, codes.Aborted:
			continue
		default:
			break
		}
	}

	return
}
