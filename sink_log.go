// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"time"
)

type sinkLog struct {
	log io.WriteCloser
}

func newSinkLog() sink {
	return sinkLog{
		log: os.Stdout,
	}
}

func newSinkLogFile(path string, sig os.Signal) (sink, error) {
	log, err := NewLogfile(path, sig)
	if (err != nil) {
		return nil, err
	}
	return sinkLog{
		log: log,
	}, nil
}

func (self sinkLog) Message(ctx context.Context, msg *LogMsg) {
	fmt.Fprintf(self.log.(io.Writer), "%s - - %s \"%s %s HTTP/1.1\" %d %s - \"%s\" cache=%s urlid=%s google_details=%s\n",
		msg.HttpRequest.RemoteIp,
		msg.Timestamp.Format(time.RFC3339),
		msg.HttpRequest.RequestMethod,
		msg.HttpRequest.RequestUrl,
		msg.HttpRequest.Status,
		msg.HttpRequest.ResponseSize,
		msg.HttpRequest.UserAgent,
		msg.Info.Cache,
		msg.Info.Urlid,
		msg.JsonPayload.StatusDetails,
	)
}
