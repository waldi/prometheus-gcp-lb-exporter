// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"errors"
	"net/url"
	"strings"
)

type mapUrl map[*url.URL]string

func (i mapUrl) Set(value string) error {
	s := strings.SplitN(value, "=", 2)
	if len(s) < 2 {
		return errors.New("")
	}
	url, err := url.Parse(s[0])
	if err != nil {
		return err
	}
	i[url] = s[1]
	return nil
}

func (i mapUrl) String() string {
	return ""
}

func (i mapUrl) Type() string {
	return "key=value"
}

func matchScheme(pattern, scheme string) bool {
	if pattern == "" {
		return true
	}
	return scheme == pattern
}

func matchHost(pattern, host string) bool {
	n := len(pattern)
	m := len(host)
	if pattern == "" {
		return true
	}
	if pattern[0] != ',' {
		return pattern == host
	}
	return m >= n && host[m-n:] == pattern
}

func matchPath(pattern, path string) bool {
	n := len(pattern)
	m := len(path)
	if pattern[n-1] != '/' {
		return pattern == path
	}
	return m >= n && path[0:n] == pattern
}

func (self mapUrl) match(match *url.URL) (urlid string) {
	matchedScheme := make(mapUrl)
	matchedHost := make(mapUrl)

	for k, v := range self {
		if matchScheme(k.Scheme, match.Scheme) {
			matchedScheme[k] = v
		}
	}

	for k, v := range matchedScheme {
		if matchHost(k.Host, match.Host) {
			matchedHost[k] = v
		}
	}

	var n = 0
	for k, v := range matchedHost {
		if (matchPath(k.Path, match.Path)) {
			if urlid == "" || len(k.Path) > n {
				n = len(k.Path)
				urlid = v
			}
		}
	}

	return
}
