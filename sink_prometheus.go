// Copyright 2017 Bastian Blank <waldi@debian.org>
// See LICENSE file.

package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const handler = "gcp-lb"

type sinkPrometheus struct {
	srv *http.Server
	registry *prometheus.Registry
	metricRequests *prometheus.CounterVec
	metricResponseSize *prometheus.HistogramVec
	notify chan os.Signal
}

func newSinkPrometheus(addr string, sig os.Signal) sink {
	sink := sinkPrometheus{
		registry: prometheus.NewRegistry(),
		notify:   make(chan os.Signal, 1),
	}

	sink.registry.MustRegister(prometheus.NewProcessCollector(os.Getpid(), ""))
	sink.registry.MustRegister(prometheus.NewGoCollector())

	sink.metricRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Number of HTTP requests",
	}, []string{"handler", "urlid", "cache", "google_details", "method", "code"})
	sink.metricResponseSize = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_response_size_bytes",
		Help: "Size of HTTP responses",
		Buckets: []float64{1000,10000,100000,1000000,10000000,100000000},
	}, []string{"handler", "urlid", "cache", "google_details"})
	sink.registry.MustRegister(sink.metricRequests)
	sink.registry.MustRegister(sink.metricResponseSize)

	mux := &http.ServeMux{}
	mux.Handle("/metrics", promhttp.HandlerFor(sink.registry, promhttp.HandlerOpts{}))
	sink.srv = &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	go sink.srv.ListenAndServe()

	go func() {
		signal.Notify(sink.notify, sig)

		for _ = range sink.notify {
			sink.metricRequests.Reset()
			sink.metricResponseSize.Reset()
			log.Print("Metrics reset")
		}
	}()


	return sink
}

func (self sinkPrometheus) Message(ctx context.Context, msg *LogMsg) {
	urlid := msg.Info.Urlid
	cache := msg.Info.Cache
	details := msg.JsonPayload.StatusDetails
	method := strings.ToLower(msg.HttpRequest.RequestMethod)
	size, _ := strconv.Atoi(msg.HttpRequest.ResponseSize)
	code := strconv.Itoa(msg.HttpRequest.Status)

	requests, err := self.metricRequests.GetMetricWithLabelValues(handler, urlid, cache, details, method, code)
	if (err != nil) {
		log.Panic("Failed to get metric vector", err)
	}
	requests.Inc()

	response_size, err := self.metricResponseSize.GetMetricWithLabelValues(handler, urlid, cache, details)
	if (err != nil) {
		log.Panic("Failed to get metric vector", err)
	}
	response_size.Observe(float64(size))
}
